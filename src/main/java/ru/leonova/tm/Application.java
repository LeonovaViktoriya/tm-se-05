package ru.leonova.tm;

import ru.leonova.tm.bootstrap.Bootstrap;
import ru.leonova.tm.command.*;

public class Application {

    private static final Class[] classes = {HelpCommand.class, ProjectCreateCommand.class,
            ProjectDeleteCommand.class, ProjectShowListCommand.class, ProjectUpdateCommand.class, TaskCreateCommand.class,
            TaskUpdateNameCommand.class, TaskDeleteCommand.class, TaskDeleteAllOfProjectCommand.class, TaskDeleteListCommand.class,
            TaskShowListCommand.class, ProjectDeleteListCommand.class, ExitCommand.class};

    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(classes);
    }

}
