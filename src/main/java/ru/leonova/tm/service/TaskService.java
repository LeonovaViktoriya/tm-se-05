package ru.leonova.tm.service;

import ru.leonova.tm.entity.Task;
import ru.leonova.tm.repository.TaskRepository;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void createTask(Task t) {
        if (t == null) return;
        if (taskRepository.findAll().isEmpty()) {
            taskRepository.persist(t);
            return;
        }
        taskRepository.merge(t);
    }

    public Collection<Task> getTaskList() {
        return taskRepository.findAll();
    }

    public void updateTaskName(String taskId, String taskName) {
        Task task = taskRepository.findOne(taskId);
        if (task == null || taskName == null || taskName.isEmpty()) return;
        task.setName(taskName);
    }

    public void deleteTasksByIdProject(String projectId) {
        if (projectId.isEmpty()) return;
        Collection<Task> taskCollection = taskRepository.findAll();
        taskCollection.removeIf(task -> task.getProjectId().equals(projectId));
    }

    public boolean isEmptyTaskList() {
        return taskRepository.findAll().isEmpty();
    }

    public void deleteTask(String taskId) {
        Task task = taskRepository.findOne(taskId);
        if (task == null) return;
        taskRepository.remove(task);
    }

    public void deleteAllTask() {
        taskRepository.removeAll();
    }

}
