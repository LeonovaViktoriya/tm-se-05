package ru.leonova.tm.service;

import ru.leonova.tm.entity.Project;
import ru.leonova.tm.repository.ProjectRepository;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class ProjectService {

    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Collection<Project> getProjectList() {
        return projectRepository.findAll();
    }

    public void createProject(Project project) {
        if (project == null) return;
        if (projectRepository.findAll().isEmpty()) {
            projectRepository.persist(project);
            return;
        }
        projectRepository.merge(project);
    }

    public void updateProject(String projectId, String name){
        Project project = findProjectById(projectId);
        if (project == null || name == null || name.isEmpty()) return;
        project.setName(name);
    }

    public boolean isEmptyProjectList() {
        return projectRepository.findAll().isEmpty();
    }

    public Project findProjectById(String id){
        if (id.isEmpty()) return null;
        return projectRepository.findOne(id);
    }

    public void deleteProject(String projectId) {
        Project project = findProjectById(projectId);
        if (project == null) return;
        projectRepository.remove(project);
    }

    public void deleteAllProject() {
        projectRepository.removeAll();
    }
}
