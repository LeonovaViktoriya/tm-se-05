package ru.leonova.tm.command;

import ru.leonova.tm.entity.Project;

public class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "create-p";
    }

    @Override
    public String getDescription() {
        return "Create project";
    }

    @Override
    public void execute() {
        System.out.print("[CREATE PROJECT]\nINTER NAME: \n");
        Project project = new Project(getScanner().nextLine());
        bootstrap.getProjectService().createProject(project);
        System.out.println("Project created");
    }
}
