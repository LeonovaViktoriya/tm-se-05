package ru.leonova.tm.command;

public class TaskDeleteListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "del-all-t";
    }

    @Override
    public String getDescription() {
        return "Delete list tasks";
    }

    @Override
    public void execute(){
        System.out.println("["+getDescription().toUpperCase()+"]");
        bootstrap.getTaskService().deleteAllTask();
        System.out.println("All tasks remove");
    }
}
