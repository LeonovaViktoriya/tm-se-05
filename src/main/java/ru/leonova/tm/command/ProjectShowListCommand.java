package ru.leonova.tm.command;

import ru.leonova.tm.entity.Project;

import java.util.Collection;

public class ProjectShowListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "list-p";
    }

    @Override
    public String getDescription() {
        return "Show project list";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        if (bootstrap.getProjectService().isEmptyProjectList()) return;
        Collection<Project> projectCollection = bootstrap.getProjectService().getProjectList();
        int i=0;
        for (Project project:projectCollection) {
            i++;
            System.out.println(i + ". ID PROJECT: " + project.getPrID() + ", NAME: " + project.getName());
        }
    }
}
