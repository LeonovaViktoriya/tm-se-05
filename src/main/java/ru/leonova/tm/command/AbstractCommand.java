package ru.leonova.tm.command;

import ru.leonova.tm.bootstrap.Bootstrap;

import java.util.Scanner;

public abstract class AbstractCommand {
    Bootstrap bootstrap;

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public Bootstrap getBootstrap() {
        return bootstrap;
    }

    Scanner getScanner(){
        return new Scanner(System.in);
    }
}
