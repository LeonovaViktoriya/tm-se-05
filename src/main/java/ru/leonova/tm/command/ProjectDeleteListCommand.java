package ru.leonova.tm.command;

public class ProjectDeleteListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "del-all-p";
    }

    @Override
    public String getDescription() {
        return "Delete list projects";
    }

    @Override
    public void execute() {
        bootstrap.getTaskService().deleteAllTask();
        bootstrap.getProjectService().deleteAllProject();
        System.out.println("All projects remove");
    }
}
