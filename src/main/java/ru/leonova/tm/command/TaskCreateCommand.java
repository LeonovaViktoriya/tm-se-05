package ru.leonova.tm.command;

import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;

import java.util.Collection;
import java.util.Scanner;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "create-t";
    }

    @Override
    public String getDescription() {
        return "Create task";
    }

    @Override
    public void execute() {
        if (bootstrap.getProjectService().isEmptyProjectList()) return;
        System.out.println("[CREATE TASK]\nList projects:");
        Collection<Project> projectCollection = bootstrap.getProjectService().getProjectList();
        int i=0;
        for (Project project:projectCollection) {
            i++;
            System.out.println(i + ". ID PROJECT: " + project.getPrID() + ", NAME: " + project.getName());
        }
        System.out.println("\nSELECT ID PROJECT: ");
        String id = getScanner().nextLine();
        System.out.println("Enter name task: ");
        Scanner input = new Scanner(System.in);
        String name = input.nextLine();
        Task t = new Task(name, id);
        bootstrap.getTaskService().createTask(t);
        System.out.println("Task created");
    }
}
