package ru.leonova.tm.command;

import ru.leonova.tm.entity.Project;

import java.util.Collection;

public class TaskDeleteAllOfProjectCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "del-all-t-t-p";
    }

    @Override
    public String getDescription() {
        return "Delete all task of the selected project";
    }

    @Override
    public void execute() {
        if(bootstrap.getProjectService().isEmptyProjectList())return;
        System.out.println("[DELETE ALL TASKS OF THE SELECTED PROJECT]\n");
        Collection<Project> projectCollection = bootstrap.getProjectService().getProjectList();
        int i=0;
        for (Project project:projectCollection) {
            i++;
            System.out.println(i + ". ID PROJECT: " + project.getPrID() + ", NAME: " + project.getName());
        }
        System.out.println("Select ID project: ");
        bootstrap.getTaskService().deleteTasksByIdProject(getScanner().nextLine());
        System.out.println("All tasks of this project are deleted");
    }
}
