package ru.leonova.tm.command;

public class HelpCommand extends AbstractCommand {

    @Override
    public final String getName() {
        return "HELP";
    }

    @Override
    public final String getDescription() {
        return "Show all commands";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command: bootstrap.getCommands()) {
            System.out.println(command.getName() + ": " + command.getDescription());
        }
    }
}

