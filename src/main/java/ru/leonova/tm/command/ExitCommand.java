package ru.leonova.tm.command;

public class ExitCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "EXIT";
    }

    @Override
    public String getDescription() {
        return "Exit the application";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}

