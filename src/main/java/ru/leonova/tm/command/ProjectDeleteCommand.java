package ru.leonova.tm.command;

import ru.leonova.tm.entity.Project;

import java.util.Collection;

public class ProjectDeleteCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "del-p";
    }

    @Override
    public String getDescription() {
        return "Delete project with all his tasks";
    }

    @Override
    public void execute() {
        if (bootstrap.getProjectService().isEmptyProjectList()) return;
        System.out.println("[DELETE PROJECT BY ID]\nList projects:");
        Collection<Project> projectCollection = bootstrap.getProjectService().getProjectList();
        int i=0;
        for (Project project:projectCollection) {
            i++;
            System.out.println(i + ". ID PROJECT: " + project.getPrID() + ", NAME: " + project.getName());
        }
        System.out.println("Enter id project:");
        String id = getScanner().nextLine();
        if (id == null || id.isEmpty()) return;
        bootstrap.getTaskService().deleteTasksByIdProject(id);
        bootstrap.getProjectService().deleteProject(id);
        System.out.println("Project with his tasks are removed");
    }
}
