package ru.leonova.tm.command;

import ru.leonova.tm.entity.Task;

import java.util.Collection;

public class TaskShowListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "list-t";
    }

    @Override
    public String getDescription() {
        return "Show tasks list";
    }

    @Override
    public void execute(){
        if(bootstrap.getTaskService().isEmptyTaskList()){
            System.out.println("List tasks is empty");
            return;
        }
        System.out.println("[TASK LIST]");
        Collection<Task> taskCollection = bootstrap.getTaskService().getTaskList();
        int i = 0;
        for (Task task : taskCollection) {
            i++;
            System.out.println(i + ". PROJECT ID: " + task.getProjectId() + ",TASK ID: " + task.getTaskId() + ", TASK NAME: " + task.getName());
        }
    }
}
