package ru.leonova.tm.command;

import ru.leonova.tm.entity.Project;

import java.util.Collection;

public class ProjectUpdateCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "up-p";
    }

    @Override
    public String getDescription() {
        return "Update name project";
    }

    @Override
    public void execute() {
        if(bootstrap.getProjectService().isEmptyProjectList())return;
        System.out.println("[UPDATE NAME PROJECT]\nList projects");
        Collection<Project> projectCollection = bootstrap.getProjectService().getProjectList();
        int i=0;
        for (Project project:projectCollection) {
            i++;
            System.out.println(i + ". ID PROJECT: " + project.getPrID() + ", NAME: " + project.getName());
        }
        System.out.println("Enter id project:");
        String projectId = getScanner().nextLine();
        if (projectId == null || projectId.isEmpty()) return;
        System.out.println("Enter new name for this project:");
        String name = getScanner().nextLine();
        if (name == null || name.isEmpty()) return;
        bootstrap.getProjectService().updateProject(projectId, name);
        System.out.println("Task modified like " + name);
    }
}
