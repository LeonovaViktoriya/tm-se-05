package ru.leonova.tm.command;

import ru.leonova.tm.entity.Task;

import java.util.Collection;

public class TaskDeleteCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "del-t";
    }

    @Override
    public String getDescription() {
        return "Delete task";
    }

    @Override
    public void execute() {
        if (bootstrap.getTaskService().isEmptyTaskList()) return;
        System.out.println("[DELETE TASK BY ID]\n[Task list:]");
        Collection<Task> taskCollection = bootstrap.getTaskService().getTaskList();
        int i = 0;
        for (Task task : taskCollection) {
            i++;
            System.out.println(i + ". PROJECT ID: " + task.getProjectId() + ",TASK ID: " + task.getTaskId() + ", TASK NAME: " + task.getName());
        }
        System.out.println("Enter task id:");
        String taskId = getScanner().nextLine();
        if (taskId == null || taskId.isEmpty()) return;
        bootstrap.getTaskService().deleteTask(taskId);
        System.out.println("Task deleted");
    }
}
