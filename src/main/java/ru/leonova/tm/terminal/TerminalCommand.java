package ru.leonova.tm.terminal;

public enum TerminalCommand {

    CREAT_PROJECT("create-p"),
    CREAT_TASK("create-t"),
    SHOW_PROJECT_LIST("list-p"),
    SHOW_TASKS_LIST("list-t"),
    DELETE_PROJECT_BYID("del-p"),
    DELETE_TASK_BYID("del-t-id"),
    UPDATE_PROJECT("up-p"),
    UPDATE_TASK("up-t"),
    DELETE_ALL_PROJECTS("del-all-p"),
    DELETE_ALL_TASKS("del-all-t"),
    DELETE_ALL_PROJECT_TASKS("del-all-t-t-p"),
    HELP("help");

    private String command;

     TerminalCommand(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

}
