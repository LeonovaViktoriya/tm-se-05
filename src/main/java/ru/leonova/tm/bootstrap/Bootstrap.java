package ru.leonova.tm.bootstrap;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.terminal.TerminalCommand;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.repository.ProjectRepository;
import ru.leonova.tm.repository.TaskRepository;
import ru.leonova.tm.service.ProjectService;
import ru.leonova.tm.service.TaskService;

import java.util.*;

public class Bootstrap {

    private final Scanner scanner = new Scanner(System.in);
    private ProjectRepository projectRepository = new ProjectRepository();
    private TaskRepository taskRepository = new TaskRepository();
    private ProjectService projectService = new ProjectService(projectRepository);
    private TaskService taskService = new TaskService(taskRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public Bootstrap() {
    }

    private void registry(AbstractCommand command) throws Exception {
        String cliCommand = command.getName();
        String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty()) throw new Exception("It is not command");
        if (cliDescription == null || cliDescription.isEmpty()) throw new Exception("Not description");
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }

    private void registry(Class... classes) throws Exception {
        for (Class clazz : classes) {
            if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
            Object command = clazz.newInstance();
            AbstractCommand abstractCommand = (AbstractCommand) command;
            registry(abstractCommand);
        }
    }

    public void init(Class... classes) throws Exception {
        if (classes == null || classes.length == 0) throw new Exception("Empty list");
        registry(classes);
        start();
    }

    private void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command;
        do {
            command = scanner.nextLine();
            execute(command);
        } while (!command.equals("exit"));
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

}

