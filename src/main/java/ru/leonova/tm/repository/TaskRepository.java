package ru.leonova.tm.repository;

import ru.leonova.tm.entity.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TaskRepository {

    private Map<String, Task> taskMap = new LinkedHashMap();

    public void persist(Task task) {
        taskMap.put(task.getTaskId(), task);
    }

    public void merge(Task task){
        for (Task t : taskMap.values()) {
            if(t.getTaskId().equals(task.getTaskId())){
                t.setName(task.getName());
            }else {
                persist(task);
            }
        }
    }

    public Collection<Task> findAll(){
        return taskMap.values();
    }

    public Task findOne(String taskId) {
        return taskMap.get(taskId);
    }

    public void remove(Task t) {
        taskMap.remove(t.getTaskId());
    }

    public void removeAll() {
        taskMap.clear();
    }

}

