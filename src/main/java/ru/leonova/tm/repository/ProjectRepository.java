package ru.leonova.tm.repository;

import ru.leonova.tm.entity.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {

    private Map<String, Project> projectMap = new LinkedHashMap();

    public void persist(Project p) {
        projectMap.put(p.getPrID(), p);
    }

    public Collection<Project> findAll() {
        return projectMap.values();
    }

    public Project findOne(String id) {
        return projectMap.get(id);
    }

    private void updateProjectName(String projectId, String name) {
        Project project = projectMap.get(projectId);
        if(project!=null) project.setName(name);
    }

    public void merge(Project p) {
        for (Project project : findAll()) {
            if (project.getPrID().equals(p.getPrID())) {
                updateProjectName(project.getPrID(), p.getName());
            } else {
                persist(p);
            }
        }
    }

    public void remove(Project p) {
        projectMap.remove(p.getPrID());
    }

    public void removeAll() {
        projectMap.clear();
    }

}
